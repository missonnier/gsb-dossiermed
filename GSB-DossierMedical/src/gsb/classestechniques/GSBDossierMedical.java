/***********************************************************************
 * Module:  GSBDossierMedical.java
 * Author:  fabrice
 * Purpose: Classe Main
 ***********************************************************************/
package gsb.classestechniques;

public class GSBDossierMedical {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //appel de la couche d'accès aux données
        AccesDonnees ad = new AccesDonnees();
        ad.instancierLesDifferentsObjets();
    }
}
