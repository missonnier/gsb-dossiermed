/***********************************************************************
 * Module:  AccesDonnees.java
 * Author:  fabrice
 * Purpose: Classe d'accès aux données (couche métier)
 ***********************************************************************/
package gsb.classestechniques;
import gsb.classesmetier.Region;

public class AccesDonnees {
    
    /**
     * Cette méthode instancierLesDifferentsObjets() permet d'accéder aux données (objets) créés. 
     * Ecrire ici le code demandé dans les questions de la mission
    */
    public void instancierLesDifferentsObjets(){
        // C'EST ICI QU'ON COMMENCE A PROGRAMMER ! 
        Region gua = new Region ("01", "Guadeloupe");
    }
}
