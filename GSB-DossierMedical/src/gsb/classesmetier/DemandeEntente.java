/***********************************************************************
 * Module:  DemandeEntente.java
 * Author:  Fabrice
 * Purpose: Defines the Class DemandeEntente
 ***********************************************************************/
package gsb.classesmetier;
import java.util.*;

public class DemandeEntente {
    private Date date;
    private Date dateFin;
    private String reponse;
    private Dossier redige;

    public Dossier getRedige() {
       return redige;
    }

    public void setRedige(Dossier newDossier) {
       this.redige = newDossier;
    }

}