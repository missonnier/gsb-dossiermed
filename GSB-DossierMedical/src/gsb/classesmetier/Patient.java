/***********************************************************************
 * Module:  Patient.java
 * Author:  Fabrice
 * Purpose: Defines the Class Patient
 ***********************************************************************/

package gsb.classesmetier;
import java.util.Date;

public class Patient {
    private int numero;
    private String nom;
    private String prenom;
    private String numss;
    private String rue;
    private String ville;
    private Date dateNaissance;
    private boolean beneficiaireCMU;
    private Commune commune;

    //TODO : QUESTION 20
    public Patient(int numero, String nom, String prenom, String numss, String rue, 
            Date dateNaissance, boolean beneficiaireCMU, Commune commune) {
        this.numero = numero;
        this.nom = nom;
        this.prenom = prenom;
        this.numss = numss;
        this.rue = rue;
        this.dateNaissance = dateNaissance;
        this.beneficiaireCMU = beneficiaireCMU;
        this.commune = commune;
    }

    public Patient(int numero, String nom, String prenom, String numss, String rue, 
            Date dateNaissance, boolean beneficiaireCMU) {
        this.numero = numero;
        this.nom = nom;
        this.prenom = prenom;
        this.numss = numss;
        this.rue = rue;
        this.dateNaissance = dateNaissance;
        this.beneficiaireCMU = beneficiaireCMU;
        this.commune = null;
    }
    
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNumss() {
        return numss;
    }

    public void setNumss(String numss) {
        this.numss = numss;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public boolean isBeneficiaireCMU() {
        return beneficiaireCMU;
    }

    public void setBeneficiaireCMU(boolean beneficiaireCMU) {
        this.beneficiaireCMU = beneficiaireCMU;
    }
    
    public Commune getCommune() {
       return commune;
    }

    public void setCommune(Commune newCommune) {
       this.commune = newCommune;
    }

  
    @Override
    public String toString() {
        return "Patient{" + "numero=" + numero + ", nom=" + nom + ", prenom=" + prenom + ", numss=" + numss + ", rue=" + rue + ", ville=" + ville + ", dateNaissance=" + dateNaissance + ", beneficiaireCMU=" + beneficiaireCMU + ", commune=" + commune + '}';
    }

    
}