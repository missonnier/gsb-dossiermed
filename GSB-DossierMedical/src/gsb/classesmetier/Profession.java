/***********************************************************************
 * Module:  Profession.java
 * Author:  Fabrice
 * Purpose: Defines the Class Profession
 ***********************************************************************/

package gsb.classesmetier;
import java.io.Serializable;

public class Profession implements Serializable{
    private String code;
    private String nom;

    public Profession(String code, String nom) {
        this.code = code;
        this.nom = nom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Profession{" + "code=" + this.code + ", nom=" + this.nom + '}';
    }
    
}
