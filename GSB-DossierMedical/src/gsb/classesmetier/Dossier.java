/***********************************************************************
 * Module:  Dossier.java
 * Author:  fabrice
 * Purpose: Defines the Class Dossier
 ***********************************************************************/
package gsb.classesmetier;
import java.util.*;


public class Dossier {

    private String numero;
    private Date dateCreation;
    private String commentaires;
    private Patient patient;
    private ProfessionnelSante traitePar;
    private ProfessionnelSante prescritPar;
    private HashMap<Soin, Frequence> lesActes;

    public Dossier(String numero, Date dateCreation, String commentaires, Patient unPatient) {
        this.numero = numero;
        this.dateCreation = dateCreation;
        this.commentaires = commentaires;
        this.patient = unPatient;
        this.traitePar = null;
        this.traitePar = null;
        this.lesActes = null;
    }

    public Patient getPatient() {
       return patient;
    }

    public void setPatient(Patient newPatient) {
       this.patient = newPatient;
    }
  
    public ProfessionnelSante getTraitePar() {
       return traitePar;
    }

    public void setTraitePar(ProfessionnelSante newProfessionnelSante) {
       this.traitePar = newProfessionnelSante;
    }

    public ProfessionnelSante getPrescritPar() {
       return prescritPar;
    }

    public void setPrescritPar(ProfessionnelSante newProfessionnelSante) {
       this.prescritPar = newProfessionnelSante;
    }
   
    public void setActe(Soin unActe, Frequence uneFrequence) {
       this.lesActes.put(unActe, uneFrequence);
    }
   
    public Frequence getFrequence(Soin unActe) {
       return this.lesActes.get(unActe);
    }
}