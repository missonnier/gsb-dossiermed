/***********************************************************************
 * Module:  Commune.java
 * Author:  Fabrice 
 * Purpose: classe Commune
 ***********************************************************************/

package gsb.classesmetier;

public class Commune {
    private String code;
    private String nom;
    private String nomMaj;
    private String codePostal;
    private String coordonneesGPS;
    private Departement leDepartement;

    public Commune(String code, String nom, String nomMaj, String codePostal, String coordonneesGPS, Departement leDepartement) {
        this.code = code;
        this.nom = nom;
        this.nomMaj = nomMaj;
        this.codePostal = codePostal;
        this.coordonneesGPS = coordonneesGPS;
        this.leDepartement = leDepartement;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNomMaj() {
        return nomMaj;
    }

    public void setNomMaj(String nomMaj) {
        this.nomMaj = nomMaj;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getCoordonneesGPS() {
        return coordonneesGPS;
    }

    public void setCoordonneesGPS(String coordonneesGPS) {
        this.coordonneesGPS = coordonneesGPS;
    }

    public Departement getLeDepartement() {
        return leDepartement;
    }

    public void setLeDepartement(Departement leDepartement) {
        this.leDepartement = leDepartement;
    }

    @Override
    public String toString() {
        return "Commune{" + "code=" + this.code + ", nom=" + this.nom + ", nomMaj=" + this.nomMaj + ", codePostal=" + this.codePostal + ", coordonneesGPS=" + this.coordonneesGPS 
                + ", leDepartement=" + this.leDepartement + '}';
    }
   
}