    /***********************************************************************
 * Module:  ProfessionnelSante.java
 * Author:  Fabrice
 * Purpose: Defines the Class ProfessionnelSante 
 ***********************************************************************/
package gsb.classesmetier;
import java.util.ArrayList;


public class ProfessionnelSante{
    private String identifiantPP;
    private String identifiantNatPP;
    private String nom;
    private String prenom;
    private String libelleCivilExercice; //Docteur par exemple
    private CategPro laCategorieProf;
    private ArrayList<Structure> exerce;
    private Profession laProfession;

    public ProfessionnelSante(String identifiantPP, String identifiantNatPP, String nom, String prenom, String libelleCivilExercice) {
        this.identifiantPP = identifiantPP;
        this.identifiantNatPP = identifiantNatPP;
        this.nom = nom;
        this.prenom = prenom;
        this.libelleCivilExercice = libelleCivilExercice;
        this.laCategorieProf = null;
        this.laProfession = null;
        this.exerce = new ArrayList();
    }
    
    public ProfessionnelSante(String identifiantPP, String identifiantNatPP, String nom, String prenom, String libelleCivilExercice, CategPro laCategorieProf, Profession laProfession) {
        this.identifiantPP = identifiantPP;
        this.identifiantNatPP = identifiantNatPP;
        this.nom = nom;
        this.prenom = prenom;
        this.libelleCivilExercice = libelleCivilExercice;
        this.laCategorieProf = laCategorieProf;
        this.laProfession = laProfession;
        this.exerce = new ArrayList();
    }
    
    public String getIdentifiantPP() {
        return identifiantPP;
    }

    public void setIdentifiantPP(String identifiantPP) {
        this.identifiantPP = identifiantPP;
    }

    public String getIdentifiantNatPP() {
        return identifiantNatPP;
    }

    public void setIdentifiantNatPP(String identifiantNatPP) {
        this.identifiantNatPP = identifiantNatPP;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public CategPro getLaCategorieProf() {
        return laCategorieProf;
    }

    public void setLaCategorieProf(CategPro laCategorieProf) {
        this.laCategorieProf = laCategorieProf;
    }

    public void setUneStructure(Structure laStructure) {
        if (this.exerce.contains(laStructure) == false) {
            this.exerce.add(laStructure);
        }
    }

    public Profession getLaProfession() {
        return laProfession;
    }

    public void setLaProfession(Profession laProfession) {
        this.laProfession = laProfession;
    }

    public String getLibelleCivilExercice() {
        return libelleCivilExercice;
    }

    public void setLibelleCivilExercice(String libelleCivilExercice) {
        this.libelleCivilExercice = libelleCivilExercice;
    }

    
}
