/***********************************************************************
 * Module:  Structure.java
 * Author:  Fabrice
 * Purpose: Defines the Class Structure (établissement médical)
 ***********************************************************************/

package gsb.classesmetier;

public class Structure{
    private String identifiantStruct;
    private String cptDest ;
    private String cptPointGeo ;
    private String numVoie ;  
    private String indRepVoie ;
    private String codeTypeVoie ;
    private String libelTypeVoie ;
    private String libelVoie ;
    private String mentionDist ;
    private String burCedex ;
    private String tel1 ;
    private String tel2 ;
    private String telecopie ;
    private String mail ;
    private String numSIRET;
    private String numSIREN;
    private String numFINESS;
    private String numFINESSEtabJur;  
    private String raisonSociale;
    private String enseigneComm ;
    private Commune laCommune;
  
    public Structure(String identifiantStruct, String cptDest, String cptPointGeo, String numVoie, String indRepVoie, String codeTypeVoie, String libelTypeVoie, String libelVoie, String mentionDist, String burCedex, String tel1, String tel2, String telecopie, String mail, String numSIRET, String numSIREN, String numFINESS, String numFINESSEtabJur, String raisonSociale, String enseigneComm, Commune laCommune) {
        this.identifiantStruct = identifiantStruct;
        this.cptDest = cptDest;
        this.cptPointGeo = cptPointGeo;
        this.numVoie = numVoie;
        this.indRepVoie = indRepVoie;
        this.codeTypeVoie = codeTypeVoie;
        this.libelTypeVoie = libelTypeVoie;
        this.libelVoie = libelVoie;
        this.mentionDist = mentionDist;
        this.burCedex = burCedex;
        this.tel1 = tel1;
        this.tel2 = tel2;
        this.telecopie = telecopie;
        this.mail = mail;
        this.numSIRET = numSIRET;
        this.numSIREN = numSIREN;
        this.numFINESS = numFINESS;
        this.numFINESSEtabJur = numFINESSEtabJur;
        this.raisonSociale = raisonSociale;
        this.enseigneComm = enseigneComm;
        this.laCommune = laCommune;
    }
    
    public Structure(String identifiantStruct, String cptDest, String cptPointGeo, String numVoie, String indRepVoie, String codeTypeVoie, String libelTypeVoie, String libelVoie, String mentionDist, String burCedex, String tel1, String tel2, String telecopie, String mail, String numSIRET, String numSIREN, String numFINESS, String numFINESSEtabJur, String raisonSociale, String enseigneComm) {
        this.identifiantStruct = identifiantStruct;
        this.cptDest = cptDest;
        this.cptPointGeo = cptPointGeo;
        this.numVoie = numVoie;
        this.indRepVoie = indRepVoie;
        this.codeTypeVoie = codeTypeVoie;
        this.libelTypeVoie = libelTypeVoie;
        this.libelVoie = libelVoie;
        this.mentionDist = mentionDist;
        this.burCedex = burCedex;
        this.tel1 = tel1;
        this.tel2 = tel2;
        this.telecopie = telecopie;
        this.mail = mail;
        this.numSIRET = numSIRET;
        this.numSIREN = numSIREN;
        this.numFINESS = numFINESS;
        this.numFINESSEtabJur = numFINESSEtabJur;
        this.raisonSociale = raisonSociale;
        this.enseigneComm = enseigneComm;
        this.laCommune = null;
    }
    
   
    @Override
    public String toString() {
        return "Structure{" + "identifiantStruct=" + identifiantStruct + ", cptDest=" + cptDest + ", cptPointGeo=" + cptPointGeo + ", numVoie=" + numVoie + ", indRepVoie=" + indRepVoie + ", codeTypeVoie=" + codeTypeVoie + ", libelTypeVoie=" + libelTypeVoie + ", libelVoie=" + libelVoie + ", mentionDist=" + mentionDist + ", burCedex=" + burCedex + ", tel1=" + tel1 + ", tel2=" + tel2 + ", telecopie=" + telecopie + ", mail=" + mail + ", numSIRET=" + numSIRET + ", numSIREN=" + numSIREN + ", numFINESS=" + numFINESS + ", numFINESSEtabJur=" + numFINESSEtabJur + ", raisonSociale=" + raisonSociale + ", enseigneComm=" + enseigneComm + ", laCommune=" + laCommune + '}';
    }
    
}
