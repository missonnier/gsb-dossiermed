/***********************************************************************
 * Module:  Departement.java
 * Author:  Fabrice
 * Purpose: Defines the Class Departement
 ***********************************************************************/

package gsb.classesmetier;


public class Departement {
    private String code;
    private String nom;
    private Region laRegion;

    public Departement(String code, String nom) {
        this.code = code;
        this.nom = nom;
        this.laRegion = null;
    }
    
    public Departement(String code, String nom, Region laRegion) {
        this.code = code;
        this.nom = nom;
        this.laRegion = laRegion;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Region getLaRegion() {
        return laRegion;
    }

    public void setLaRegion(Region laRegion) {
        this.laRegion = laRegion;
    }

    @Override
    public String toString() {
        if(this.laRegion == null){
            return "Departement{" + "code=" + this.code + ", nom=" + this.nom + ", pas de région associée }";
        }
        else{
            return "Departement{" + "code=" + this.code + ", nom=" + this.nom + ", laRegion=" + this.laRegion.toString() + '}';
        }
            
    }

}
