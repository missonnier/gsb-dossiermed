/***********************************************************************
 * Module:  CategPro.java
 * Author:  Fabrice
 * Purpose: classe Catégorie professionnelle d'un prof. de santé
 ***********************************************************************/

package gsb.classesmetier;

public class CategPro{
    private String code;
    private String nom;

    public CategPro(String code, String nom) {
        this.code = code;
        this.nom = nom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "CategPro{" + "code=" + this.code + ", nom=" + this.nom + '}';
    }
   
}
