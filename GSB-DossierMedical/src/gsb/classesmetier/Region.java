/***********************************************************************
 * Module:  Region.java
 * Author:  Fabrice
 * Purpose: classe Region
 ***********************************************************************/

package gsb.classesmetier;

public class Region {
    private String code;
    private String nom;

    public Region(String code, String nom) {
        this.code = code;
        this.nom = nom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Region{" + "code= " + this.code + ", nom=" + this.nom + '}';
    }
}
